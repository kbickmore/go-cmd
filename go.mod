module gitlab.com/kbickmore/go-cmd

go 1.14

require (
	github.com/c-bata/go-prompt v0.2.4-0.20200321140817-d043be076398
	github.com/mattn/go-colorable v0.1.6 // indirect
	github.com/mattn/go-runewidth v0.0.9 // indirect
	github.com/mattn/go-tty v0.0.3 // indirect
	github.com/pkg/term v0.0.0-20200520122047-c3ffed290a03 // indirect
)

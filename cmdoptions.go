package cmd

import (
	prompt "github.com/c-bata/go-prompt"
)

// ConsoleParser ...
type ConsoleParser = prompt.ConsoleParser

// ConsoleWriter ...
type ConsoleWriter = prompt.ConsoleWriter

// Color represents color on terminal.
type Color = prompt.Color

const (
	// DefaultColor represents a default color.
	DefaultColor Color = iota

	// Low intensity

	// Black represents a black.
	Black
	// DarkRed represents a dark red.
	DarkRed
	// DarkGreen represents a dark green.
	DarkGreen
	// Brown represents a brown.
	Brown
	// DarkBlue represents a dark blue.
	DarkBlue
	// Purple represents a purple.
	Purple
	// Cyan represents a cyan.
	Cyan
	// LightGray represents a light gray.
	LightGray

	// High intensity

	// DarkGray represents a dark gray.
	DarkGray
	// Red represents a red.
	Red
	// Green represents a green.
	Green
	// Yellow represents a yellow.
	Yellow
	// Blue represents a blue.
	Blue
	// Fuchsia represents a fuchsia.
	Fuchsia
	// Turquoise represents a turquoise.
	Turquoise
	// White represents a white.
	White
)

// Key is the type express the key inserted from user.
type Key = prompt.Key

const (
	// Escape Key ...
	Escape Key = iota

	ControlA
	ControlB
	ControlC
	ControlD
	ControlE
	ControlF
	ControlG
	ControlH
	ControlI
	ControlJ
	ControlK
	ControlL
	ControlM
	ControlN
	ControlO
	ControlP
	ControlQ
	ControlR
	ControlS
	ControlT
	ControlU
	ControlV
	ControlW
	ControlX
	ControlY
	ControlZ

	ControlSpace
	ControlBackslash
	ControlSquareClose
	ControlCircumflex
	ControlUnderscore
	ControlLeft
	ControlRight
	ControlUp
	ControlDown

	Up
	Down
	Right
	Left

	ShiftLeft
	ShiftUp
	ShiftDown
	ShiftRight

	Home
	End
	Delete
	ShiftDelete
	ControlDelete
	PageUp
	PageDown
	BackTab
	Insert
	Backspace

	// Aliases.
	Tab
	Enter
	// Actually Enter equals ControlM, not ControlJ,
	// However, in prompt_toolkit, we made the mistake of translating
	// \r into \n during the input, so everyone is now handling the
	// enter key by binding ControlJ.

	// From now on, it's better to bind `ASCII_SEQUENCES.Enter` everywhere,
	// because that's future compatible, and will still work when we
	// stop replacing \r by \n.

	F1
	F2
	F3
	F4
	F5
	F6
	F7
	F8
	F9
	F10
	F11
	F12
	F13
	F14
	F15
	F16
	F17
	F18
	F19
	F20
	F21
	F22
	F23
	F24

	// Any Matches any key.
	Any

	// Special
	CPRResponse
	Vt100MouseEvent
	WindowsMouseEvent
	BracketedPaste

	// Key which is ignored. (The key binding for this key should not do anything.)
	Ignore

	// Key is not defined
	NotDefined
)

// KeyBindFunc receives buffer and processed it.
// func(*buffer)
type KeyBindFunc = prompt.KeyBindFunc

// KeyBindMode to switch a key binding flexibly.
// string
type KeyBindMode = prompt.KeyBindMode

const (
	// CommonKeyBind is a mode without any keyboard shortcut
	CommonKeyBind KeyBindMode = "common"
	// EmacsKeyBind is a mode to use emacs-like keyboard shortcut
	EmacsKeyBind KeyBindMode = "emacs"
)

// KeyBind is a struct which represents which key should do what operation.
//  Key key
//	Fn KeyBindFunc }
type KeyBind = prompt.KeyBind

// ASCIICodeBind is a struct which represents which []byte should do what operation
//  ASCIICode []byte
//	Fn        KeyBindFunc}
type ASCIICodeBind = prompt.ASCIICodeBind

// Document has text displayed in terminal and cursor position. This struct has
// 	Text string
// 	// This represents a index in a rune array of Document.Text.
// 	// So if Document is "日本(cursor)語", cursorPosition is 2.
// 	// But DisplayedCursorPosition returns 4 because '日' and '本' are double width characters.
// 	cursorPosition int
// 	lastKey        Key
type Document = prompt.Document

// Option is the type to replace default parameters.
// shell.NewShell accepts any number of options (this is functional option pattern).
type Option = prompt.Option

// OptionPrefixTextColor change a text color of prefix string. func(x Color) Option
var OptionPrefixTextColor = prompt.OptionPrefixTextColor

// OptionPrefixBackgroundColor to change a background color of prefix string. func(x Color) Option
var OptionPrefixBackgroundColor = prompt.OptionPrefixBackgroundColor

// OptionInputTextColor to change a color of text which is input by user. func(x Color) Option
var OptionInputTextColor = prompt.OptionInputTextColor

// OptionInputBGColor to change a color of background which is input by user. func(x Color) Option
var OptionInputBGColor = prompt.OptionInputBGColor

// OptionPreviewSuggestionTextColor to change a text color which is completed. func(x Color) Option
var OptionPreviewSuggestionTextColor = prompt.OptionPreviewSuggestionTextColor

// OptionPreviewSuggestionBGColor to change a background color which is completed. func(x Color) Option
var OptionPreviewSuggestionBGColor = prompt.OptionPreviewSuggestionBGColor

// OptionSuggestionTextColor to change a text color in drop down suggestions. func(x Color) Option
var OptionSuggestionTextColor = prompt.OptionSuggestionTextColor

// OptionSuggestionBGColor change a background color in drop down suggestions. func(x Color) Option
var OptionSuggestionBGColor = prompt.OptionSuggestionBGColor

// OptionSelectedSuggestionTextColor to change a text color for completed text which is selected inside suggestions drop down box. func(x Color) Option
var OptionSelectedSuggestionTextColor = prompt.OptionSelectedSuggestionTextColor

// OptionSelectedSuggestionBGColor to change a background color for completed text which is selected inside suggestions drop down box. func(x Color) Option
var OptionSelectedSuggestionBGColor = prompt.OptionSelectedSuggestionBGColor

// OptionDescriptionTextColor to change a background color of description text in drop down suggestions. func(x Color) Option
var OptionDescriptionTextColor = prompt.OptionDescriptionTextColor

// OptionDescriptionBGColor to change a background color of description text in drop down suggestions. func(x Color) Option
var OptionDescriptionBGColor = prompt.OptionDescriptionBGColor

// OptionSelectedDescriptionTextColor to change a text color of description which is selected inside suggestions drop down box. func(x Color) Option
var OptionSelectedDescriptionTextColor = prompt.OptionSelectedDescriptionTextColor

// OptionSelectedDescriptionBGColor to change a background color of description which is selected inside suggestions drop down box. func(x Color) Option
var OptionSelectedDescriptionBGColor = prompt.OptionSelectedDescriptionBGColor

// OptionScrollbarThumbColor to change a thumb color on scrollbar. func(x Color) Option
var OptionScrollbarThumbColor = prompt.OptionScrollbarThumbColor

// OptionScrollbarBGColor to change a background color of scrollbar. func(x Color) Option
var OptionScrollbarBGColor = prompt.OptionScrollbarBGColor

// OptionParser to set a custom ConsoleParser object. An argument should implement ConsoleParser interface. func(x ConsoleParser) Option
var OptionParser = prompt.OptionParser

// OptionWriter to set a custom ConsoleWriter object. An argument should implement ConsoleWriter interface.  func(x ConsoleWriter) Option
var OptionWriter = prompt.OptionWriter

// OptionTitle to set title displayed at the header bar of terminal. func(x string) Option
var OptionTitle = prompt.OptionTitle

// OptionPrefix to set prefix string. func(x string) Option
var OptionPrefix = prompt.OptionPrefix

// OptionInitialBufferText to set the initial buffer text. func(x string) Option
var OptionInitialBufferText = prompt.OptionInitialBufferText

// OptionCompletionWordSeparator to set word separators. Enable only ' ' if empty. func(x string) Option
var OptionCompletionWordSeparator = prompt.OptionCompletionWordSeparator

// OptionLivePrefix to change the prefix dynamically by callback function.  func(f func() (prefix string, useLivePrefix bool)) Option
var OptionLivePrefix = prompt.OptionLivePrefix

// OptionMaxSuggestion specify the max number of displayed suggestions.
// Example: OptionMaxSuggestion(5)
var OptionMaxSuggestion = prompt.OptionMaxSuggestion

// OptionHistory to set history expressed by string array.
// Example: OptionHistory([]string{"SELECT * FROM users;"})
var OptionHistory = prompt.OptionHistory

// OptionCompletionOnDown allows for Down arrow key to trigger completion.
// Example: OptionCompletionOnDown()
var OptionCompletionOnDown = prompt.OptionCompletionOnDown

// OptionShowCompletionAtStart to set completion window is open at start.
// OptionShowCompletionAtStart()
var OptionShowCompletionAtStart = prompt.OptionShowCompletionAtStart

// OptionSwitchKeyBindMode set a key bind mode.
// select key bindings either "emacs" or "common"
// Example: OptionSwitchKeyBindMode("common")
var OptionSwitchKeyBindMode = prompt.OptionSwitchKeyBindMode

// OptionAddKeyBind to set a custom key bind.
// OptionAddKeyBind(b ...KeyBind)
var OptionAddKeyBind = prompt.OptionAddKeyBind

// OptionAddASCIICodeBind to set a custom key bind.
// OptionAddASCIICodeBind(b ...ASCIICodeBind)
var OptionAddASCIICodeBind = prompt.OptionAddASCIICodeBind

// OptionBreakLineCallback to run a callback at every break line
// OptionBreakLineCallback(fn func(*Document))
var OptionBreakLineCallback = prompt.OptionBreakLineCallback

// ExitChecker is called after user input to check if prompt must stop and exit go-prompt Run loop.
// User input means: selecting/typing an entry, then, if said entry content matches the ExitChecker function criteria:
// - immediate exit (if breakline is false) without executor called
// - exit after typing <return> (meaning breakline is true), and the executor is called first, before exit.
// Exit means exit go-prompt (not the overall Go program)
// func(in string, breakline bool) bool
type ExitChecker = prompt.ExitChecker

// OptionSetExitCheckerOnInput set an exit function which checks if go-prompt exits its Run loop
// OptionSetExitCheckerOnInput(fn ExitChecker)
var optionSetExitCheckerOnInput = prompt.OptionSetExitCheckerOnInput

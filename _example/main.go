package main

import (
	"fmt"

	cmd "gitlab.com/kbickmore/go-cmd"
)

func test(args []string) bool {
	fmt.Println(args)
	return false
}

func test2(args []string) bool {
	fmt.Println(args)
	return true
}

func main() {
	var lol2Args = []cmd.Suggestion{
		{Text: "first arg", Description: "first desc"},
		{Text: "second arg", Description: "second desc"},
	}

	var test2OptArgs = map[string]cmd.OptArg{
		"-t":     {Description: "an option", Arguments: nil},
		"-l":     {Description: "not really", Arguments: nil},
		"--lol":  {Description: "not really", Arguments: nil},
		"--lol2": {Description: "not really", Arguments: lol2Args},
	}
	var commands = cmd.Command{
		Function:    test2,
		Description: "my other function",
		Options:     test2OptArgs,
	}

	var cmdMap = cmd.CommandMap{
		"test":  cmd.Command{test, "my function", nil},
		"test2": commands,
	}

	cmdShell := cmd.NewCmd(cmdMap,
		cmd.OptionInputTextColor(cmd.Yellow),
		cmd.OptionPrefixTextColor(cmd.Fuchsia),
		cmd.OptionInputBGColor(cmd.Blue),
	)

	cmdShell.MainLoop("starting up...\n")

}

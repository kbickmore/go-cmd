package cmd

import (
	"strings"

	prompt "github.com/c-bata/go-prompt"
)

func (s Cmd) complete(d prompt.Document) []Suggestion {
	if d.TextBeforeCursor() == "" {
		return []Suggestion{}
	}

	args := strings.Split(d.TextBeforeCursor(), " ")
	w := d.GetWordBeforeCursor()

	if len(args) <= 1 {
		return prompt.FilterHasPrefix(s.commandStrings, args[0], true)
	}
	if strings.HasPrefix(w, "--") {
		// fmt.Println("found --")
		return s.optionCompleter(args, strings.HasPrefix(w, "--"))
	} else if strings.HasPrefix(w, "-") {
		return s.optionCompleter(args, false)
	} else if arguments, found := s.getOptionArg(args, d); found {
		return arguments
	}

	return []Suggestion{}
}

func getLastOption(args []string, d prompt.Document) (opt string, found bool) {
	numArgs := len(args)

	if numArgs >= 2 {
		opt = args[numArgs-2]
	}
	if strings.HasPrefix(opt, "-") {
		return opt, true
	}
	return "", false
}

func (s Cmd) getOptionArg(args []string, d prompt.Document) (optionArgs []Suggestion, found bool) {
	opt, found := getLastOption(args, d)

	if !found {
		return
	}

	optionArgs = s.commands[args[0]].Options[opt].Arguments
	return
}

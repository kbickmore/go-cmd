package cmd

import (
	"strings"

	prompt "github.com/c-bata/go-prompt"
)

var optionHelp = []Suggestion{
	{Text: "-h"},
	{Text: "--help"},
}

func (s Cmd) optionCompleter(args []string, long bool) []Suggestion {
	l := len(args)
	if l <= 1 {
		if long {
			return prompt.FilterHasPrefix(optionHelp, "--", false)
		}
		return optionHelp
	}

	var suggests []Suggestion
	suggests = s.omap[args[0]]

	if long {
		suggests = prompt.FilterContains(
			prompt.FilterHasPrefix(suggests, "--", false),
			strings.TrimLeft(args[l-1], "--"),
			true,
		)
	}
	suggests = excludeOptions(args, suggests)

	return suggests
}

func excludeOptions(args []string, suggests []Suggestion) (newSuggests []Suggestion) {
	var found bool

	for _, nsuggest := range suggests {
		found = false
		for _, arg := range args {
			if strings.Compare(arg, nsuggest.Text) != 0 {
				continue
			} else {
				found = true
			}
		}
		if !found {
			newSuggests = append(newSuggests, nsuggest)
		}
	}

	return
}

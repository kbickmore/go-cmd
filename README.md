# go-cmd

![Go Report Card](https://goreportcard.com/badge/gitlab.com/kbickmore/go-cmd)
![Software License](https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square)
[![GoDoc](https://godoc.org/gitlab.com/kbickmore/go-cmd?status.svg)](https://godoc.org/gitlab.com/kbickmore/go-cmd)

A Library that provides a simple framework for writing line-oriented command interpreters. Inspired primarily by the python [cmd](https://docs.python.org/3/library/cmd.html) module. 

This is primarily a wrapper around the [go-prompt](https://github.com/c-bata/go-prompt) module that simply makes it easier to use. Providing users with its auto-completion features, history, customization features, and choice of emacs or standard key bindings.

```go
package main

import (
	"fmt"
	cmd "gitlab.com/kbickmore/go-cmd"
)

func test(args []string) bool {
	fmt.Println(args)
	return false
}

func test2(args []string) bool {
	fmt.Println(args)
	return true
}

func main() {
	var lol2Args = []cmd.Suggestion{
		{Text: "first arg", Description: "first desc"},
		{Text: "second arg", Description: "second desc"},
	}

	var test2OptArgs = map[string]cmd.OptArg{
		"-t":     {Description: "an option", Arguments: nil},
		"-l":     {Description: "one more", Arguments: nil},
		"--nextarg":  {Description: "still", Arguments: nil},
		"--anotherone": {Description: "more", Arguments: lol2Args},
	}
	var commands = cmd.Command{
		Function:    test2,
		Description: "my other function",
		Options:     test2OptArgs,
	}

	var cmdMap = cmd.CommandMap{
		"test":  cmd.Command{test, "my function", nil},
		"test2": commands,
	}

	cmdShell := cmd.NewCmd(cmdMap,
		cmd.OptionInputTextColor(shell.Yellow),
		cmd.OptionPrefixTextColor(shell.Fuchsia),
		cmd.OptionInputBGColor(shell.Blue),
	)

	cmdShell.MainLoop("starting up...\n")

}
```

## Links

* [Change Log](./CHANGELOG.md)
* [GoDoc](https://godoc.org/gitlab.com/kbickmore/go-cmd)
* [GoPkg](https://pkg.go.dev/gitlab.com/kbickmore/go-cmd?tab=doc)

## Author

Kristopher Bickmore

## License

This software is licensed under the MIT license, see [LICENSE](./LICENSE) for more information.

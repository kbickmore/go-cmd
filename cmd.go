//Package cmd allows developers to easily build command line interpreters, inspired by the python cmd module.
package cmd

import (
	"fmt"
	"os"
	"strings"

	prompt "github.com/c-bata/go-prompt"
)

// Suggestion is equivalent to go-prompt.Suggest {Text, Description}
type Suggestion = prompt.Suggest

// OptArg Represents options available for commands.
type OptArg struct {
	Description string
	Arguments   []Suggestion
}

// Command Represents a terminal command.  The Function is required.
type Command struct {
	Function    func(args []string) (quit bool)
	Description string
	Options     map[string]OptArg
}

// CommandMap A mapping of the string names to Command structures
type CommandMap map[string]Command

type optionMap map[string][]Suggestion

// Cmd The core structure of go-cmd
type Cmd struct {
	commands       CommandMap
	Precmd         func(line string)
	Postcmd        func(stop bool, line string)
	Preloop        func()
	Postloop       func()
	omap           optionMap
	p              *prompt.Prompt
	stdin          *os.File
	stdout         *os.File
	stop           bool
	commandStrings []Suggestion
}

// NewCmd Creates and returns a new Cmd struct. Must be called before anything else.
func NewCmd(cmdMap CommandMap, opts ...Option) (cmd Cmd) {
	cmd.commands = cmdMap

	cmd.commandStrings = make([]Suggestion, len(cmdMap))
	cmd.omap = make(optionMap, len(cmdMap))
	numOptions := 0

	i := 0
	for name, command := range cmd.commands {
		cmd.commandStrings[i].Text = name
		cmd.commandStrings[i].Description = command.Description

		for oname, optarg := range cmd.commands[name].Options {
			cmd.omap[name] = append(cmd.omap[name], Suggestion{Text: oname, Description: optarg.Description})
		}
		numOptions += len(cmd.omap[name])

		i++
	}

	cmd.Precmd = defaultPrecmd
	cmd.Preloop = defaultPreloop
	cmd.Postcmd = defauiltPostcmd
	cmd.Postloop = defauiltPostloop

	opts = append(opts, optionSetExitCheckerOnInput(cmd.exitChecker))
	cmd.p = prompt.New(
		cmd.executor,
		cmd.complete,
		opts...,
	)

	return
}

// MainLoop Begins the actual command line prompt.
func (cmd *Cmd) MainLoop(intro string) {
	cmd.Preloop()

	if intro != "" {
		fmt.Println(intro)
	}

	cmd.p.Run()

	cmd.Postloop()
}

func parseLine(line string) (command string, args []string) {
	s := strings.TrimSpace(line)
	if s == "" {
		return
	}

	splitline := strings.Split(s, " ")
	return strings.TrimSpace(splitline[0]), splitline

}

// Executor ...
func (cmd *Cmd) executor(line string) {
	cmd.Precmd(line)

	cmdStr, args := parseLine((line))
	if cmdStr == "" {
		return
	}

	command, ok := cmd.commands[cmdStr]
	if ok {
		cmd.stop = command.Function(args)
	} else {
		fmt.Println(cmdStr, "is an unknown command.")
	}

	cmd.Postcmd(cmd.stop, line)
}

func (cmd *Cmd) exitChecker(line string, breakline bool) bool {
	if cmd.stop {
		return true
	}
	return false
}

// AddCommand Allows you to dynamically add new Commands to the shell at run time.
func (cmd *Cmd) AddCommand(name string, command Command) {
	cmd.commands[name] = command
	cmd.commandStrings = append(cmd.commandStrings,
		Suggestion{Text: name, Description: command.Description})

	for oname, optarg := range command.Options {
		cmd.omap[name] = append(cmd.omap[name], Suggestion{Text: oname, Description: optarg.Description})
	}
}

func defaultPreloop() {}

func defauiltPostloop() {}

func defauiltPostcmd(bool, string) {}

func defaultPrecmd(string) {}
